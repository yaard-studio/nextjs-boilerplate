/** @type {import('tailwindcss').Config} */

module.exports = {
  darkMode: "class",
  content: [
    "./node_modules/flowbite-react/**/*.js",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./public/**/*.html",
  ],
  plugins: [require("flowbite/plugin")],
  theme: {
    extend: {
      colors: {
        poppy: {
          50: "#fffaeb",
          100: "#fdf1c8",
          200: "#fce28b",
          300: "#facd4f",
          400: "#f8b827",
          500: "#f2960d",
          600: "#d67109",
          700: "#b24e0b",
          800: "#903c10",
          900: "#773210",
          950: "#441804",
        },
      },
    },
  },
};
