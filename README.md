# Next.js - Tailwind & Supabase Boilerplate

This repository serves as a robust boilerplate for creating web interfaces using Next.js and Tailwind CSS, complemented by a Supabase-backed API.

## Current features

- **Authentication**
    - Email Authentication
        - Signup
        - Signin
        - Password Recovery
        - Password Reset

## Installation

```bash
git git clone git@gitlab.com:dommangetnicolas/nextjs-boilerplate.git
cd nextjs-boilerplate
rm -rf .git
git init
nvm use
yarn install
```

Create a `.env.development.local` file and populate it with the following information:
```
NEXT_PUBLIC_SUPABASE_URL=<value>
NEXT_PUBLIC_SUPABASE_ANON_KEY=<value>
NEXT_PUBLIC_SUPABASE_SERVICE_ROLE_KEY=<value>
FRONT_URL=<value>
```

Don't forget to update `app.ts` based on your project requirements, including the project name, logo, and other relevant details.

## Usage

To start the development server, run the following command:

```bash
yarn run dev
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
