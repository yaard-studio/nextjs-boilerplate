/**
 * String manipulation
 */
String.prototype.truncate = function truncate(
  limit: number,
  appendedString: string = "...",
) {
  const result =
    this.length > limit ? this.slice(0, limit - 1) + appendedString : this;

  return result.toString();
};

/**
 * Number manipulation
 */
Number.prototype.toCurrency = function toCurrency() {
  return new Intl.NumberFormat("fr-FR", {
    style: "currency",
    currency: "EUR",
    minimumFractionDigits: 0,
  }).format(Number(this.toFixed(0)));
};

Number.prototype.toSurface = function toSurface() {
  return this ? `${this}㎡` : "";
};

export {};
