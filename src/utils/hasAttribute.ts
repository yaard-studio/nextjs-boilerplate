export function hasAttribute(obj: unknown, attributeName: string): boolean {
  return (obj as any)[attributeName] !== undefined;
}
