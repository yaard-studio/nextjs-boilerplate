import { createMiddlewareClient } from "@supabase/auth-helpers-nextjs";
import { NextRequest, NextResponse } from "next/server";

import errors from "../assets/translations/en/errors.json";

const apiUnauthenticatedRoutes = [
  "/api/auth/signin",
  "/api/auth/signup",
  "/api/auth/password-forgotten",
];

export async function apiMiddleware(req: NextRequest) {
  const res = NextResponse.next();

  const supabase = createMiddlewareClient({ req, res });
  const { data } = await supabase.auth.getSession();

  if (
    data.session === null &&
    !apiUnauthenticatedRoutes.includes(req.nextUrl.pathname)
  ) {
    return NextResponse.json(
      {
        error: errors["auth-5"],
        errorCode: "auth-5",
      },
      { status: 401 },
    );
  }

  return res;
}
