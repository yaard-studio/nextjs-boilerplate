import { NextResponse } from "next/server";
import { AnyObject, ObjectSchema, ValidationError } from "yup";

export async function validateBody(
  validationSchema: ObjectSchema<AnyObject>,
  body: unknown,
) {
  let errors: string[] = [];

  try {
    await validationSchema.validate(body);
  } catch (error) {
    if (error instanceof ValidationError) {
      errors = error.errors;
    }
  } finally {
    return errors;
  }
}

export function returnValidationErrors(errors: string[]) {
  return NextResponse.json(
    {
      error: "Invalid input parameters",
      errors,
      errorCode: "input-1",
    },
    { status: 422 },
  );
}
