import { cookies } from "next/headers";

export function getHttpCookiesHeader() {
  return { Cookie: cookies().toString() };
}
