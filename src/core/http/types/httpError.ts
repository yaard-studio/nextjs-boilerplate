export type HttpError = {
  error: string;
  errorCode: string;
};
