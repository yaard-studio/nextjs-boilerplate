import errors from "../../assets/translations/en/errors.json";
import { hasAttribute } from "../../utils/hasAttribute";
import { HttpError } from "./types/httpError";

const parseResponseJson = async (response: Response): Promise<unknown> => {
  try {
    const data = await response.json();
    return data;
  } catch {}
};

export const parseHttpResponse = async <T>(response: Response) => {
  const data = await parseResponseJson(response);

  if (!response.ok) {
    if (data !== undefined) {
      throw data;
    }

    throw new Error();
  }

  return data as T;
};

export const parseHttpError = (
  error: unknown,
): { message: string; error?: HttpError } => {
  if (hasAttribute(error, "error") && hasAttribute(error, "errorCode")) {
    return {
      error: error as HttpError,
      message: (error as HttpError).error,
    };
  }

  return { message: errors["error-1"] };
};
