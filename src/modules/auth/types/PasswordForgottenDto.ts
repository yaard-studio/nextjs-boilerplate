export type PasswordForgottenDto = {
  email: string;
};
