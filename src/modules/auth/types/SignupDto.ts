export type SignupDto = {
  email: string;
  name: string;
  password: string;
};
