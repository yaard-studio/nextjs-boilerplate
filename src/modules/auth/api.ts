import { parseHttpResponse } from "../../core/http";
import { PasswordForgottenDto } from "./types/PasswordForgottenDto";
import { SigninDto } from "./types/SigninDto";
import { SignupDto } from "./types/SignupDto";

export const AuthApi = {
  signin: async (payload: SigninDto) => {
    const response = await fetch("/api/auth/signin", {
      method: "POST",
      body: JSON.stringify(payload),
    });
    return await parseHttpResponse(response);
  },
  signup: async (payload: SignupDto) => {
    const response = await fetch("/api/auth/signup", {
      method: "POST",
      body: JSON.stringify(payload),
    });
    return await parseHttpResponse(response);
  },
  passwordForgotten: async (payload: PasswordForgottenDto) => {
    const response = await fetch("/api/auth/password-forgotten", {
      method: "POST",
      body: JSON.stringify(payload),
    });
    return await parseHttpResponse(response);
  },
};
