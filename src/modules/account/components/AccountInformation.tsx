import appDayjs from "../../../core/dayjs";
import { Profile } from "../../profile/types/Profile";

type TAccountInformationProps = {
  profile: Profile;
};

function AccountInformation({ profile }: TAccountInformationProps) {
  return (
    <div className="bg-white rounded-lg p-5 border mt-3">
      <h3 className="font-semibold text-lg mb-5">My information</h3>

      <div className="flex">
        <div className="w-1/2">
          <p className="text-gray-500">Your name</p>
          <p className="text-gray-900 font-medium">{profile?.name}</p>
        </div>

        <div className="w-1/2">
          <p className="text-gray-500">Registered at</p>
          <p className="text-gray-900 font-medium">
            {appDayjs(profile?.created_at).format("L")}
          </p>
        </div>
      </div>
    </div>
  );
}

export default AccountInformation;
