import { Profile } from "../../profile/types/Profile";

type TAccountSecurityProps = {
  profile: Profile;
};

function AccountSecurity({ profile }: TAccountSecurityProps) {
  return (
    <div className="bg-white rounded-lg p-5 border mt-3">
      <h3 className="font-semibold text-lg mb-5">Security</h3>

      <div className="flex">
        <div className="w-1/2">
          <p className="text-gray-500">Your email</p>
          <p className="text-gray-900 font-medium">{profile?.email}</p>
        </div>

        <div className="w-1/2">
          <p className="text-gray-500">Your password</p>
          <p className="text-gray-900 font-medium">••••••••</p>
        </div>
      </div>
    </div>
  );
}

export default AccountSecurity;
