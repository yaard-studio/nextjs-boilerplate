import { Avatar } from "flowbite-react";

import { Profile } from "../../profile/types/Profile";

type TAccountOverviewProps = {
  profile: Profile;
};

function AccountOverview({ profile }: TAccountOverviewProps) {
  return (
    <div className="bg-white rounded-lg p-5 border flex items-center">
      <Avatar alt="Avatar of Nicolas" rounded size="md" />

      <div className="ml-3">
        <p className="font-semibold text-lg">{profile?.name}</p>
        <p className="text-gray-600">{profile?.email}</p>
      </div>
    </div>
  );
}

export default AccountOverview;
