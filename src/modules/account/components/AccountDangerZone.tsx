"use client";
import { Button } from "flowbite-react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { parseHttpError } from "../../../core/http";
import { showError } from "../../../ui/components/Toast";
import { ProfileApi } from "../../profile/api";
import AccountDeletionModal from "./AccountDeletionModal";

function AccountDangerZone() {
  const [isAccountDeletionModalOpen, setIsAccountDeletionModalOpen] =
    useState(false);
  const [isDeletingAccount, setIsDeletingAccount] = useState(false);

  const { replace } = useRouter();

  const onDeleteAccount = async () => {
    setIsDeletingAccount(true);

    try {
      await ProfileApi.delete();
      replace("/auth/signin");
    } catch (error: unknown) {
      showError(parseHttpError(error).message);
    } finally {
      setIsAccountDeletionModalOpen(false);
      setIsDeletingAccount(false);
    }
  };

  return (
    <>
      <div className="bg-white rounded-lg p-5 border border-red-500 mt-3">
        <h3 className="font-semibold text-lg mb-5">Danger zone</h3>

        <div className="flex">
          <div className="w-1/2">
            <Button
              color="failure"
              onClick={() => setIsAccountDeletionModalOpen(true)}
            >
              Delete my account
            </Button>
          </div>
        </div>
      </div>

      {isAccountDeletionModalOpen && (
        <AccountDeletionModal
          isPending={isDeletingAccount}
          onConfirm={onDeleteAccount}
          onCancel={() => setIsAccountDeletionModalOpen(false)}
        />
      )}
    </>
  );
}

export default AccountDangerZone;
