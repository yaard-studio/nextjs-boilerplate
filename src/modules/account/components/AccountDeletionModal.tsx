import { Button, Modal, Spinner } from "flowbite-react";

type AccountDeletionModalProps = {
  isPending: boolean;
  onConfirm: () => void;
  onCancel: () => void;
};

function AccountDeletionModal({
  isPending,
  onCancel,
  onConfirm,
}: AccountDeletionModalProps) {
  return (
    <Modal show onClose={onCancel}>
      <Modal.Header>Account deletion</Modal.Header>
      <Modal.Body>
        <div className="space-y-6">
          Are you sure you want to delete your account? This action is
          irreversible and will permanently erase all your data.
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button color="failure" onClick={onConfirm}>
          Delete my account{" "}
          {isPending && <Spinner color="gray" size="sm" className="ml-2" />}
        </Button>
        <Button color="gray" onClick={onCancel}>
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default AccountDeletionModal;
