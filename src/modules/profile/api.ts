import { env } from "../../core/env";
import { parseHttpResponse } from "../../core/http";
import { Profile } from "./types/Profile";

export const ProfileApi = {
  get: async (init?: RequestInit) => {
    const response = await fetch(`${env.baseUrl}/api/profile`, {
      method: "GET",
      ...init,
    });
    return await parseHttpResponse<Profile>(response);
  },
  delete: async () => {
    const response = await fetch("/api/profile", {
      method: "DELETE",
      body: JSON.stringify({ password: "dummy-todo" }),
    });
    await parseHttpResponse(response);
  },
};
