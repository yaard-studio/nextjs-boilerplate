import classNames from "classnames";
import {
  AnchorHTMLAttributes,
  ComponentProps,
  DetailedHTMLProps,
  FunctionComponent,
} from "react";

type SidebarItemProps = DetailedHTMLProps<
  AnchorHTMLAttributes<HTMLAnchorElement>,
  HTMLAnchorElement
> & {
  icon: FunctionComponent<ComponentProps<"svg">>;
};

function SidebarItem({
  children,
  className,
  icon: Icon,
  ...props
}: SidebarItemProps) {
  return (
    <li>
      <a
        className={classNames(
          "flex items-center justify-center rounded-lg p-2 text-base font-normal text-gray-900 hover:bg-gray-200",
          className,
        )}
        {...props}
      >
        <Icon />
        <span className="px-3 flex-1 whitespace-nowrap">{children}</span>
      </a>
    </li>
  );
}

export default SidebarItem;
