import { HiCheck, HiOutlineExclamationCircle } from "react-icons/hi";
import { toast } from "sonner";

export function showMessage(message: string) {
  toast(message);
}

export function showSuccess(message: string) {
  toast(message, { icon: <HiCheck color="#0E9F6E" size={24} /> });
}

export function showError(message: string) {
  toast(message, {
    icon: (
      <HiOutlineExclamationCircle className="mr-3" color="#F05252" size={18} />
    ),
  });
}
