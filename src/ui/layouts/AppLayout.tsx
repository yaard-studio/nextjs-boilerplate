import React, { PropsWithChildren } from "react";
import AppSidebar from "./AppSidebar";

type TAppLayoutProps = PropsWithChildren & {};

function AppLayout({ children }: TAppLayoutProps) {
  return (
    <div className="flex">
      <div className="h-screen sticky top-0">
        <AppSidebar />
      </div>

      <div className="flex-1">{children}</div>
    </div>
  );
}

export default AppLayout;
