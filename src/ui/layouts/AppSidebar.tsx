import Image from "next/image";
import {
  HiOutlineCollection,
  HiOutlineLogout,
  HiOutlineUserCircle,
} from "react-icons/hi";

import { app } from "../../../app";
import SidebarItem from "../components/SidebarItem";

function AppSidebar() {
  return (
    <nav aria-label="Sidebar" className="h-full w-64">
      <div className="h-full overflow-y-auto overflow-x-hidden bg-white border-r py-4 px-3">
        <a
          href="/"
          className="flex items-center mb-10 p-2 font-semibold text-gray-900"
        >
          <Image className="w-8 h-8 mr-2" src={app.logo} alt="logo" />
          {app.name}
        </a>

        <div>
          <ul
            data-testid="flowbite-sidebar-item-group"
            className="mt-4 space-y-2 border-t border-gray-200 pt-4 first:mt-0 first:border-t-0 first:pt-0"
          >
            <SidebarItem href="/" icon={HiOutlineCollection}>
              Dashboard
            </SidebarItem>

            <SidebarItem href="/account" icon={HiOutlineUserCircle}>
              Account
            </SidebarItem>
            <SidebarItem
              href="/auth/signout"
              icon={HiOutlineLogout}
              className="cursor-pointer"
            >
              Sign out
            </SidebarItem>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default AppSidebar;
