import { CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import { PropsWithChildren } from "react";

const customTheme: CustomFlowbiteTheme = {
  button: {
    color: {
      primary:
        "text-white bg-poppy-500 border border-transparent enabled:hover:bg-poppy-400 focus:ring-4 focus:ring-poppy-300",
    },
  },
};

function ThemeProvider({ children }: PropsWithChildren) {
  return <Flowbite theme={{ theme: customTheme }}>{children}</Flowbite>;
}

export default ThemeProvider;
