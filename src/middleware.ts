import type { NextRequest } from "next/server";
import { apiMiddleware } from "./middlewares/apiMiddleware";
import { appMiddleware } from "./middlewares/appMiddleware";

export async function middleware(req: NextRequest) {
  if (req.nextUrl.pathname.startsWith("/api")) {
    return apiMiddleware(req);
  }

  return appMiddleware(req);
}

export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - api (API routes)
     * - _next/static (static files)
     * - _next/image (image optimization files)
     * - favicon.ico (favicon file)
     */
    "/((?!_next/static|_next/image|favicon.ico).*)",
  ],
};
