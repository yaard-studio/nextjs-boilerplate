import { getHttpCookiesHeader } from "../../core/http/functions/getHttpCookiesHeader";
import AccountDangerZone from "../../modules/account/components/AccountDangerZone";
import AccountInformation from "../../modules/account/components/AccountInformation";
import AccountOverview from "../../modules/account/components/AccountOverview";
import AccountSecurity from "../../modules/account/components/AccountSecurity";
import { ProfileApi } from "../../modules/profile/api";
import AppLayout from "../../ui/layouts/AppLayout";

const AccountPage = async () => {
  const profile = await ProfileApi.get({ headers: getHttpCookiesHeader() });

  return (
    <AppLayout>
      <div className="p-10">
        <h1 className="font-semibold text-2xl mb-5">Account</h1>

        <div className="bg-white rounded-lg p-5">
          <AccountOverview profile={profile} />

          <AccountInformation profile={profile} />

          <AccountSecurity profile={profile} />

          <AccountDangerZone />
        </div>
      </div>
    </AppLayout>
  );
};

export default AccountPage;
