"use client";
import { PropsWithChildren } from "react";
import { Toaster } from "sonner";

import "../assets/css/globals.css";
import ThemeProvider from "../ui/providers/ThemeProvider";

export default function RootLayout({ children }: PropsWithChildren) {
  return (
    <ThemeProvider>
      <html lang="en" className="h-full bg-gray-50">
        <body className="h-full">
          {children}
          <Toaster />
        </body>
      </html>
    </ThemeProvider>
  );
}
