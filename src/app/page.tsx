"use client";
import AppLayout from "../ui/layouts/AppLayout";

const HomePage = () => {
  return (
    <AppLayout>
      <h1>Dashboard</h1>
    </AppLayout>
  );
};

export default HomePage;
