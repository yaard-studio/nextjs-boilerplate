"use client";

import { Button, Label, Spinner, TextInput } from "flowbite-react";
import { useFormik } from "formik";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { object, string } from "yup";

import { app } from "../../../../app";
import { AuthApi } from "../../../modules/auth/api";
import { showError, showSuccess } from "../../../ui/components/Toast";
import { parseHttpError } from "../../../core/http";

type FormValues = {
  email: string;
  name: string;
  password: string;
};

const validationSchema = object({
  email: string().required(),
  name: string().min(3).required(),
  password: string().min(8).required(),
});

export default function SignupPage() {
  const { replace } = useRouter();

  const onSubmit = async (values: FormValues) => {
    try {
      await AuthApi.signup(values);

      showSuccess("Your account has been created");

      replace("/");
    } catch (error: unknown) {
      showError(parseHttpError(error).message);
    }
  };

  const {
    handleBlur,
    handleChange,
    handleSubmit,
    values,
    touched,
    errors,
    isSubmitting,
  } = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
    },
    onSubmit,
    validationSchema,
  });

  return (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
      <a
        href="#"
        className="flex items-center mb-6 text-2xl font-semibold text-gray-900"
      >
        <Image className="w-8 h-8 mr-2" src={app.logo} alt="logo" />
        {app.name}
      </a>
      <div className="w-full bg-white rounded-lg border md:mt-0 sm:max-w-md xl:p-0">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
            Sign up to your account
          </h1>

          <form
            className="flex max-w-md flex-col gap-4"
            onSubmit={handleSubmit}
          >
            <div>
              <div className="mb-2 block">
                <Label htmlFor="name" value="Your name" />
              </div>
              <TextInput
                id="name"
                type="text"
                name="name"
                placeholder="Auguste Bartholdi"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                {...(errors.name !== undefined &&
                  touched.name && { color: "failure" })}
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="email" value="Your email" />
              </div>
              <TextInput
                id="email"
                type="email"
                name="email"
                placeholder="me@mail.com"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                {...(errors.email !== undefined &&
                  touched.email && { color: "failure" })}
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="password" value="Your password" />
              </div>
              <TextInput
                id="password"
                type="password"
                name="password"
                placeholder="••••••••"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                {...(errors.password !== undefined &&
                  touched.password && { color: "failure" })}
              />
            </div>

            <Button type="submit" color="primary" disabled={isSubmitting}>
              Sign up
              {isSubmitting && (
                <Spinner color="gray" size="sm" className="ml-2" />
              )}
            </Button>

            <p className="text-sm font-light text-gray-500">
              Already have an account?{" "}
              <Link
                href="/auth/signin"
                className="font-medium text-primary-600 hover:underline"
              >
                Sign in
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}
