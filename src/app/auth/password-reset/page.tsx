"use client";

import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import { Button, Label, Spinner, TextInput } from "flowbite-react";
import { useFormik } from "formik";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { object, string } from "yup";

import { app } from "../../../../app";
import { parseHttpError } from "../../../core/http";
import { showError, showSuccess } from "../../../ui/components/Toast";

type FormValues = {
  password: string;
};

const validationSchema = object({
  password: string().min(8).required(),
});

export default function PasswordResetPage() {
  const supabase = createClientComponentClient();

  const { replace } = useRouter();

  const onSubmit = async (values: FormValues) => {
    try {
      await supabase.auth.updateUser({ password: values.password });

      showSuccess("Your password has been reset");

      replace("/");
    } catch (error: unknown) {
      showError(parseHttpError(error).message);
    }
  };

  const {
    handleBlur,
    handleChange,
    handleSubmit,
    values,
    touched,
    errors,
    isSubmitting,
  } = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit,
    validationSchema,
  });

  return (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
      <a
        href="#"
        className="flex items-center mb-6 text-2xl font-semibold text-gray-900"
      >
        <Image className="w-8 h-8 mr-2" src={app.logo} alt="logo" />
        {app.name}
      </a>
      <div className="w-full bg-white rounded-lg border md:mt-0 sm:max-w-md xl:p-0">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
            Password reset
          </h1>

          <form
            className="flex max-w-md flex-col gap-4"
            onSubmit={handleSubmit}
          >
            <div>
              <div className="mb-2 block">
                <Label htmlFor="email" value="Your new password" />
              </div>
              <TextInput
                id="password"
                type="password"
                name="password"
                placeholder="••••••••"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                {...(errors.password !== undefined &&
                  touched.password && { color: "failure" })}
              />
            </div>

            <Button type="submit" color="primary" disabled={isSubmitting}>
              Set a new password
              {isSubmitting && (
                <Spinner color="gray" size="sm" className="ml-2" />
              )}
            </Button>
          </form>
        </div>
      </div>
    </div>
  );
}
