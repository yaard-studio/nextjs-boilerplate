import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";
import { AuthError } from "@supabase/supabase-js";
import { cookies } from "next/headers";
import type { NextRequest } from "next/server";
import { NextResponse } from "next/server";

import { env } from "../../../core/env";
import { Database } from "../../../database/database.types";
import errors from "../../../assets/translations/en/errors.json";

export async function GET(request: NextRequest) {
  const requestUrl = new URL(request.url);
  const code = requestUrl.searchParams.get("code");

  if (code === null) {
    return NextResponse.json(
      {
        error: errors["auth-3"],
        errorCode: "auth-3",
      },
      { status: 422 },
    );
  }

  const cookieStore = cookies();
  const supabase = createRouteHandlerClient<Database>({
    cookies: () => cookieStore,
  });

  try {
    await supabase.auth.exchangeCodeForSession(code);
  } catch (err) {
    if (err instanceof AuthError && err.status === 403) {
      return NextResponse.json(
        {
          error: errors["auth-4"],
          errorCode: "auth-4",
        },
        { status: 403 },
      );
    }
  }

  return NextResponse.redirect(env.frontUrl);
}
