"use client";

import { Button, Label, Spinner, TextInput } from "flowbite-react";
import { useFormik } from "formik";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { object, string } from "yup";

import { app } from "../../../../app";
import { parseHttpError } from "../../../core/http";
import { AuthApi } from "../../../modules/auth/api";
import { showError } from "../../../ui/components/Toast";

type FormValues = {
  email: string;
  password: string;
};

const validationSchema = object({
  email: string().required(),
  password: string().min(8).required(),
});

export default function SigninPage() {
  const { replace } = useRouter();

  const onSubmit = async (values: FormValues) => {
    try {
      await AuthApi.signin(values);

      replace("/");
    } catch (error: unknown) {
      showError(parseHttpError(error).message);
    }
  };

  const {
    handleBlur,
    handleChange,
    handleSubmit,
    values,
    touched,
    errors,
    isSubmitting,
  } = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit,
    validationSchema,
  });

  return (
    <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
      <a
        href="#"
        className="flex items-center mb-6 text-2xl font-semibold text-gray-900"
      >
        <Image className="w-8 h-8 mr-2" src={app.logo} alt="logo" />
        {app.name}
      </a>
      <div className="w-full bg-white rounded-lg border md:mt-0 sm:max-w-md xl:p-0">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
            Sign in to your account
          </h1>

          <form
            className="flex max-w-md flex-col gap-4"
            onSubmit={handleSubmit}
          >
            <div>
              <div className="mb-2 block">
                <Label htmlFor="email" value="Your email" />
              </div>
              <TextInput
                id="email"
                type="email"
                name="email"
                placeholder="me@mail.com"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                {...(errors.email !== undefined &&
                  touched.email && { color: "failure" })}
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="password" value="Your password" />
              </div>
              <TextInput
                id="password"
                type="password"
                name="password"
                placeholder="••••••••"
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                {...(errors.password !== undefined &&
                  touched.password && { color: "failure" })}
              />
            </div>

            <div className="flex items-center justify-end">
              <Link
                href="/auth/password-forgotten"
                className="text-sm font-medium text-primary-600 hover:underline"
              >
                Forgot password?
              </Link>
            </div>

            <Button type="submit" color="primary" disabled={isSubmitting}>
              Sign in
              {isSubmitting && (
                <Spinner color="gray" size="sm" className="ml-2" />
              )}
            </Button>

            <p className="text-sm font-light text-gray-500">
              Don’t have an account yet?{" "}
              <Link
                href="/auth/signup"
                className="font-medium text-primary-600 hover:underline"
              >
                Sign up
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}
