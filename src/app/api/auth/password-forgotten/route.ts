import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import { object, string } from "yup";

import errors from "../../../../assets/translations/en/errors.json";
import { env } from "../../../../core/env";
import { returnValidationErrors, validateBody } from "../../../../core/yup";
import { Database } from "../../../../database/database.types";
import { PasswordForgottenDto } from "../../../../modules/auth/types/PasswordForgottenDto";

const validationSchema = object({
  email: string().required(),
});

export async function POST(request: Request) {
  const body: PasswordForgottenDto = await request.json();

  const validationErrors = await validateBody(validationSchema, body);

  if (validationErrors.length > 0) {
    return returnValidationErrors(validationErrors);
  }

  const cookieStore = cookies();
  const supabase = createRouteHandlerClient<Database>({
    cookies: () => cookieStore,
  });

  const { error } = await supabase.auth.resetPasswordForEmail(body.email, {
    redirectTo: `${env.frontUrl}/auth/password-reset`,
  });

  if (error !== null) {
    return NextResponse.json(
      {
        error: errors["error-1"],
        errorCode: "error-1",
      },
      { status: 500 },
    );
  }

  return new Response(null, { status: 201 });
}
