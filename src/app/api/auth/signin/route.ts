import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import { object, string } from "yup";

import errors from "../../../../assets/translations/en/errors.json";
import { returnValidationErrors, validateBody } from "../../../../core/yup";
import { Database } from "../../../../database/database.types";
import { SigninDto } from "../../../../modules/auth/types/SigninDto";

const validationSchema = object({
  email: string().required(),
  password: string().min(8).required(),
});

export async function POST(request: Request) {
  const body: SigninDto = await request.json();

  const validationErrors = await validateBody(validationSchema, body);

  if (validationErrors.length > 0) {
    return returnValidationErrors(validationErrors);
  }

  const cookieStore = cookies();
  const supabase = createRouteHandlerClient<Database>({
    cookies: () => cookieStore,
  });

  const { data } = await supabase.auth.signInWithPassword({
    email: body.email,
    password: body.password,
  });

  if (data.user === null) {
    return NextResponse.json(
      { error: errors["auth-1"], errorCode: "auth-1" },
      { status: 401 },
    );
  }

  const profile = await supabase
    .from("profiles")
    .select("*")
    .eq("id", data.user.id)
    .single();

  if (profile.data === null) {
    return NextResponse.json(
      { error: errors["auth-2"], errorCode: "auth-2" },
      { status: 404 },
    );
  }

  return NextResponse.json({ profile: profile.data }, { status: 200 });
}
