import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";
import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import { object, string } from "yup";

import errors from "../../../../assets/translations/en/errors.json";
import { env } from "../../../../core/env";
import { returnValidationErrors, validateBody } from "../../../../core/yup";
import { Database } from "../../../../database/database.types";
import { SignupDto } from "../../../../modules/auth/types/SignupDto";

const validationSchema = object({
  email: string().required(),
  name: string().min(3).required(),
  password: string().min(8).required(),
});

export async function POST(request: Request) {
  const body: SignupDto = await request.json();

  const validationErrors = await validateBody(validationSchema, body);

  if (validationErrors.length > 0) {
    return returnValidationErrors(validationErrors);
  }

  const cookieStore = cookies();
  const supabase = createRouteHandlerClient<Database>({
    cookies: () => cookieStore,
  });

  const { data, error: signupError } = await supabase.auth.signUp({
    email: body.email,
    password: body.password,
    options: {
      emailRedirectTo: `${env.frontUrl}/auth/callback`,
    },
  });

  if (signupError !== null && signupError.status === 400) {
    return NextResponse.json(
      {
        error: errors["auth-6"],
        errorCode: "auth-6",
      },
      { status: 409 },
    );
  }

  if (signupError !== null || data.user === null) {
    return NextResponse.json(
      {
        error: errors["error-1"],
        errorCode: "error-1",
      },
      { status: 500 },
    );
  }

  const { error: insertError } = await supabase
    .from("profiles")
    .insert({ id: data.user.id, email: body.email, name: body.name });

  if (insertError !== null) {
    return NextResponse.json(
      {
        error: errors["error-1"],
        errorCode: "error-1",
      },
      { status: 500 },
    );
  }

  return new Response(null, { status: 201 });
}
