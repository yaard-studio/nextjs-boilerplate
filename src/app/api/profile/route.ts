import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";
import { createClient } from "@supabase/supabase-js";
import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import { object, string } from "yup";

import errors from "../../../assets/translations/en/errors.json";
import { env } from "../../../core/env";
import { returnValidationErrors, validateBody } from "../../../core/yup";

const validationSchema = object({
  password: string().min(8).required(),
});

export async function GET() {
  const cookieStore = cookies();
  const supabase = createRouteHandlerClient({ cookies: () => cookieStore });

  const { data } = await supabase.auth.getSession();

  const { data: profile } = await supabase
    .from("profiles")
    .select("*")
    .eq("id", data.session?.user.id)
    .single();

  return NextResponse.json(profile, { status: 200 });
}

export async function DELETE(request: Request) {
  const body = await request.json();

  const validationErrors = await validateBody(validationSchema, body);

  if (validationErrors.length > 0) {
    return returnValidationErrors(validationErrors);
  }

  const cookieStore = cookies();
  const supabase = createRouteHandlerClient({ cookies: () => cookieStore });

  const { data } = await supabase.auth.getSession();

  if (data.session === null) {
    return NextResponse.json(
      {
        error: errors["error-1"],
        errorCode: "error-1",
      },
      { status: 500 },
    );
  }

  const supbaseAdmin = createClient(
    env.supabaseUrl,
    env.supabaseServiceRoleKey,
  );

  await supabase
    .from("profiles")
    .delete()
    .eq("id", data.session?.user.id);

  const { error } = await supbaseAdmin.auth.admin.deleteUser(
    data.session?.user.id,
  );

  if (error !== null) {
    return NextResponse.json(
      {
        error: errors["error-1"],
        errorCode: "error-1",
      },
      { status: 500 },
    );
  }

  return new Response(null, { status: 200 });
}
