declare interface String {
  truncate(limit: number, appendedString?: string): string;
}

declare interface Number {
  toCurrency(): string;
  toSurface(): string;
}
