create table public.profiles (
    id uuid not null,
    email text not null,
    name text not null,
    avatar_url text null,
    updated_at timestamp with time zone null,
    constraint profiles_pkey primary key (id),
    constraint profiles_id_fkey foreign key (id) references auth.users (id),
    constraint name check ((char_length(name) >= 3))
) tablespace pg_default;
